

public class TwoStackQueue<E> {
    
	private Stack<E> one;
	private Stack<E> two; 
	
	public TwoStackQueue(){
		
		// creates twol stacks
                one = new Stack<E>();
                two = new Stack<E>();
                

	}
	
	public void enqueue(E newData){
	
    // we put our data in one of them           
                one.push(newData);
              
                

	}
	
	public Node<E> dequeue(){
	
		// we transfer all the data of one to two and then we pop from 2. after that, we transfer back the data from 2 to one
                while(one.isEmpty()==false){
                two.push(one.pop().getData());
                }
                Node<E> temp = two.pop();

                
                while(two.isEmpty()==false){
                one.push(two.pop().getData());
                }
                
                return temp;
                     
	}
	
	public boolean isEmpty(){
	
		// Check if the Queue is empty
                if(one ==null)
                    return true;
                
                return false;

	}
	
	public void printQueue(){

		// Loop through your queue and print each Node's data
                while(one.isEmpty()==false){
                two.push(one.pop().getData());
                }
                two.printStack();
	}
}



