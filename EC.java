


import java.util.Scanner;


public class EC {
	public static void main(String[] args){
	
		// Create a Scanner that reads system input
		
		// Loop over the scanner's input
		// For each line of the input, send it to isPalindrome()
		// If isPalindrome returns true, print "This is a Palindrome." 
		// Otherwise print "Not a Palindrome."
		
		// Close the Scanner	
				Scanner number= new Scanner(System.in);
                //Scanner palindromeCheck = new Scanner(System.in);
                //System.out.println("Length? ");
                int length; 
				String input;
				//number.close();
				length = number.nextInt();
				number.nextLine();
                
                
                for(int i = 0; i<length; i++)
                {
                    input = number.nextLine(); 
                    if(isPalindromeEC(input)){
                        System.out.println("This is a Palindrome.");
                    }else
                    {
                        System.out.println("Not a Palindrome.");
                    }
                
                }
	}
	
//	public static boolean isPalindrome(String s){
//	
//		// Create a stack and a Queue of chars that represents the passed in string
//		// Hint: While you loop through the given string, push the same char onto your stack
//		//		 that you enqueue into your Queue. This way you can use dequeue to get 
//		//       the string from left to right, but you pop the string from right to left
//		
//		// Compare your Queue and Stack to see if the input String was a Palindrome or not
//                
//                Queue<Character> queue = new Queue<Character>();
//                Stack<Character> stack = new Stack<Character>();
//                
//                char[] arrChar = s.toCharArray();
//                for (char c : arrChar) {
//                    stack.push(c);
//                    queue.enqueue(c);
//                }
////                stack.printStack();
////                queue.printQueue();
//                
//                boolean isPalindrome = true;
//                for (int i = 0; i < arrChar.length/2; i++) {
////                      stack.printStack();   debugging
////                      queue.printQueue();
//                      if (stack.pop().getData()!= queue.dequeue().getData()) {
//                           isPalindrome = false;                
//                           break;
//                      }
//                }       
//                //System.out.println(isPalindrome);   debugging
//                
//                return isPalindrome;
//        }
	
	public static boolean isPalindromeEC(String s){
	
		// Implement if you wish to do the extra credit.
                TwoStackQueue<Character> queue = new TwoStackQueue<Character>();
                Stack<Character> stack = new Stack<Character>();
                
                char[] arrChar = s.toCharArray();
                for (char c : arrChar) {
                    stack.push(c);
                    queue.enqueue(c);
                }

                
                boolean isPalindrome = true;
                for (int i = 0; i < arrChar.length/2; i++) {

                      if (stack.pop().getData()!= queue.dequeue().getData()) {
                           isPalindrome = false;                
                           break;
                      }
                }       
                
                return isPalindrome;
		
   }
}
