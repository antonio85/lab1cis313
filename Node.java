

public class Node<E> {
	private E data;
	private Node<E> next;
	
	public Node(E c, Node<E> n){// starts the node
	
		// Set the data and next fields to the corresponding input
		data = c;
        next = n;
	}
	
	public void setData(E d){// setter
		
		// Set the "data" data field to the corresponding input
                data =d;
	}
	
	public void setNext(Node<E> n){//setter next

		// Set the "next" data field to the corresponding input
                next = n;
	}
	public E getData(){

		// Return the "data" data field
                return data;
		
	}
	
	public Node<E> getNext(){
	
		// return the "next" data field
                return next;
		
	}
	
}

